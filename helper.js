/*
 * @Author: VINAY SINGH BAGHEL 
 * @Date: 2020-10-21 07:30:34 
 * @Last Modified by: VINAY SINGH BAGHEL 
 * @Last Modified time: 2020-10-21 07:30:34 
 */
/*********************************
CORE PACKAGES
**********************************/
const mongoose = require('mongoose');
const q = require('q');
const async = require('async');
const _ = require('underscore');
const network = require('network');
const nodemailer = require('nodemailer');
const configAuth =require('./config/auth.js');
/*********************************
MODULE PACKAGES
**********************************/

/*********************************
GLOBAL Functions
**********************************/

function mergeByProperty(arr1, arr2, prop) {
    _.each(arr2, function (arr2obj) {
        var arr1obj = _.find(arr1, function (arr1obj) {
            return arr1obj[prop] === arr2obj[prop];
        });
        if (arr1obj) {
            _.extend(arr1obj, arr2obj)
        }
    });
}

function sortResults(prop, asc) {
    arr = arr.sort(function (a, b) {
        if (asc) return (a[prop] > b[prop]);
        else return (b[prop] > a[prop]);
    });
    showResults();
}

function randomStringAlpha(strLength, charSet) {
    var result = [];
    strLength = strLength || 5;
    charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    while (--strLength) {
        result.push(charSet.charAt(Math.floor(Math.random() * charSet.length)));
    }
    return result.join('');
}

function randomString(strLength, charSet) {
    var result = [];
    strLength = strLength || 5;
    charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    while (--strLength) {
        result.push(charSet.charAt(Math.floor(Math.random() * charSet.length)));
    }
    return result.join('');
}

function toBuffer(ab) {
    var buffer = new Buffer(ab.byteLength);
    var view = new Uint8Array(ab);
    for (var i = 0; i < buffer.length; ++i) {
        buffer[i] = view[i];
    }
    return buffer;
}

/*********************************
Module Package
**********************************/

var MyModule = function () {
    var getModuleName = function (queryParam) {
        var result = mongoose.modelNames();
        for (var i = 0; i < result.length; i++) {
            result[i] = result[i].toUpperCase();
        }
        return result.sort();
    }

    var encryption = function () {
        var crypto = require('crypto');
        var assert = require('assert');
        var algorithm = 'aes256'; // or any other algorithm supported by OpenSSL
        var key = 'password';
        var text = 'I love kittens';
        var cipher = crypto.createCipher(algorithm, key);
        var encrypted = cipher.update(text, 'utf8', 'hex') + cipher.final('hex');
        return encrypted;
    }

    var getSchema = function (model) {
        var coll = model.collection.name;
        var data = mongoose.model(coll).schema;
        var obj = [];
        Object.keys(data.paths).forEach(function (schema) {
            obj.push({ 'key': schema, 'dataType': data.paths[schema].instance })
        });
        return obj;
    }
    
    Array.prototype.clean = function (deleteValue) {
        for (var i = 0; i < this.length; i++) {
            if (this[i] == deleteValue) {
                this.splice(i, 1);
                i--;
            }
        }
        return this;
    };

    var dateFormat = function (date) {
        var date = new Date(date);
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        var day = date.getDate();

        if (month < 10) month = '0' + month;
        if (day < 10) day = '0' + day;

        var parseDate = day + "/" + month + "/" + year;
        return parseDate;
    }

    var dateTimeFormat = function (date) {
        var date = new Date(date);
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        var day = date.getDate();

        if (month < 10) month = '0' + month;
        if (day < 10) day = '0' + day;

        var getHours = date.getHours(); // => 9
        var getMinutes = date.getMinutes(); // =>  30
        var getSeconds = date.getSeconds(); // => 51
        var parseDate = day + "/" + month + "/" + year + " " + getHours + ":" + getMinutes + ":" + getSeconds;;
        return parseDate;
    }

    function convertToDate(data) {
        var day, month, year, date;
        var monthNames = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"
        ];
        if (data != "") {
            data = data.split('-')
            year = data[0];
            month = data[1];
            day = data[2];
            date = day + " " + monthNames[parseInt(month) - 1] + " " + year + " " + "00:00:00 -0000";
            data = new Date(date);
        }
        return data;
    }

    function convertToDateNew(data) {
        var day, month, year, date;
        var monthNames = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"
        ];
        if (data != "") {
            data = data.split('/')
            year = data[2];
            month = data[1];
            day = data[0];
            date = day + " " + monthNames[parseInt(month) - 1] + " " + year + " " + "00:00:00 -0000";
            data = new Date(date);
        }
        return data;
    }

    function convertToDateOldFormate(data) {
        if (data) {
            var year = data.substr(0, 4);
            var month = data.substr(5, 2);
            var mydate = data.substr(8, 2);
            var newDate = mydate + "/" + month + "/" + year;
            return newDate;
        } else return data;

    }

    var randomNumber = function (strLength) {
        var result = [];
        var charSet = '123456789';
        while (--strLength) {
            result.push(charSet.charAt(Math.floor(Math.random() * charSet.length)));
        }
        return result.join('');
    }

    var pad = function (a, b, c, d) {
        return a = (a || c || 0) + '', b = new Array((++b || 3) - a.length).join(c || 0), d ? a + b : b + a
    }

    function getTimeStamp() {
        var now = new Date();
        return (now.getHours() + ':' + ((now.getMinutes() < 10) ? ("0" + now.getMinutes()) : (now.getMinutes())));
    }

    function addTimes(startTime, endTime) {
        var times = [0, 0, 0]
        var max = times.length

        var a = (startTime || '').split(':')
        var b = (endTime || '').split(':')

        // normalize time values
        for (var i = 0; i < max; i++) {
            a[i] = isNaN(parseInt(a[i])) ? 0 : parseInt(a[i])
            b[i] = isNaN(parseInt(b[i])) ? 0 : parseInt(b[i])
        }

        // store time values
        for (var i = 0; i < max; i++) {
            times[i] = a[i] + b[i]
        }

        var hours = times[0]
        var minutes = times[1]
        var seconds = times[2]

        if (seconds >= 60) {
            var m = (seconds / 60) << 0
            minutes += m
            seconds -= 60 * m
        }
        if (minutes >= 60) {
            var h = (minutes / 60) << 0
            hours += h
            minutes -= 60 * h
        }
        return ('0' + hours).slice(-2) + ':' + ('0' + minutes).slice(-2)
    }

    var getDays = function () {
        // var d = new Date();
        var weekday = new Array(7);
        weekday[0] = "Sunday";
        weekday[1] = "Monday";
        weekday[2] = "Tuesday";
        weekday[3] = "Wednesday";
        weekday[4] = "Thursday";
        weekday[5] = "Friday";
        weekday[6] = "Saturday";
        return
    }

    var getCurrentDate = function () {
        let date = new Date();
        let year = date.getFullYear();
        let month = date.getMonth() + 1;
        let day = date.getDate();
        let currentDate = convertToDateNew(day + '/' + month + '/' + year);
        return;
    }
    
    return {
        dateFormat: dateFormat,
        dateTimeFormat: dateTimeFormat,
        convertToDate: convertToDate,
        randomNumber: randomNumber,
        pad: pad,
        getTimeStamp: getTimeStamp,
        encryption: encryption,
        convertToDateOldFormate: convertToDateOldFormate,
        convertToDateNew: convertToDateNew,
        addTimes: addTimes,
        getDays: getDays,
        getCurrentDate: getCurrentDate,
       
     
    }

}();



module.exports = MyModule;