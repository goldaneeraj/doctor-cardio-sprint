var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session')
// var mysql = require('mysql');
var app = express();

var routes = require('./routes/index');
// var users = require('./routes/users');
var routing = require('./routes/routing.js');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json({ limit: '50mb' }));
// app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
// app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

app.use(bodyParser.urlencoded({
    limit: '50mb',
    extended: true,
    parameterLimit: 50000
}));
app.use(cookieParser());

// app.use(session({
//     secret: 'secret spy',
//     name: "MUOnLineTest",
//     store: new MongoStore({
//         mongooseConnection: mongoose.connection
//     }),
//     proxy: true,
//     resave: true,
//     saveUninitialized: true
// }));

// app.use(session({"secret": "Suviddha"}));
app.use(session({ secret: 'keyboard cat',watch: false, cookie: { maxAge: 60000000 }}));

// app.use(session({
//     // store,
//     secret: "Suviddha",
//     // resave: true,
//     watch: false,
//     // saveUninitialized: true,
//     // cookie: {
//     //     // secure: true,
//     //     maxAge: 1000 * 60 * 60 * 24 * 7
//     // }
// }));

app.use('/', routes);
app.use('/route', routing);
// app.use('/users', users);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  res.status(404);
  // respond with html page
  if (req.accepts('html')) {
    // res.render('404', { url: req.url });
    res.send({ error: req.url +':- url Not found' });
    return;
  }
  // respond with json
  if (req.accepts('json')) {
    res.send({ error: 'Not found' });
    return;
  }
  // default to plain-text. send()
  res.type('txt').send('Not found');
});


module.exports = app;
