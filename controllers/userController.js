var express = require('express');
// express-session
var router = express.Router();
var jwt = require('jsonwebtoken');
var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
var _ = require("underscore");
var crypto = require('crypto');
const CryptoJS = require('crypto-js');
const randomString = require('random-string');

var config = require('../config');
var db = require('../dbConnection');


function createJWT(user) {
    try {
        user = JSON.parse(JSON.stringify(user));
        const payload = {
            uId: user.uId,
            uEmail: user.uEmail,
            uName:user.uName,
            uMobile:user.uMobile,
            status:user.status
        };
        const options = {
            subject: "lms",
            expiresIn: 60 * 60 * 11
        };
        const token = jwt.sign(payload, config.jwt_secret, options);
        return token;
    } catch (err) {
        console.log('err : %s', err.toString());
    }
}

module.exports = {
    getUser: function (req, res) {
        try {
            var id = req.params.id;
            if (id && id != "") {
                console.log("id---  ", id);
                var resultsNew = {}
                db.query('SELECT * FROM user WHERE uId="' + id + '" AND status = 1', function (error, results, fields) {
                    if (error) {
                        resultsNew['data'] = [];
                        resultsNew['message'] = error;
                        resultsNew['status'] = false;
                    } else {
                        if (results && results[0]) {
                            resultsNew['data'] = results[0];
                            resultsNew['status'] = true;
                        } else {
                            resultsNew['message'] = "Data not found";
                            resultsNew['data'] = [];
                            resultsNew['status'] = false;
                        }
                    }
                    return res.json(resultsNew);
                });
            } else {
                var resultsNew = {}
                resultsNew['message'] = "category Id is require";
                resultsNew['data'] = [];
                resultsNew['status'] = false;
                return res.json(resultsNew);
            }
        } catch (e) {
            console.log(e);
            return false;
        }
    },
    loginPass: function (req, res) {
        try {
            var uEmail = req.body.uEmail;
            var userPass = req.body.uPassword;
            if (userPass && uEmail) {
                // var userPass = getEncryptPass(userPass);
                console.log("userPass---  ", userPass);
                var resultsNew = {}
                db.query('SELECT * FROM user WHERE uEmail="' + uEmail + '" AND status = 1', function (error, results, fields) {
                    if (error) {
                        resultsNew['token'] = "";
                        resultsNew['data'] = [];
                        resultsNew['message'] = error;
                        resultsNew['status'] = false;
                        return res.json(resultsNew);
                    } else {
                        if (results && results[0]) {
                            var userPassword = CryptoJS.AES.decrypt(results[0].uPassword, results[0].userUid).toString(CryptoJS.enc.Utf8);
                            if (userPassword !== userPass) {
                                resultsNew['message'] = "Incorrect Password Entered."
                                resultsNew['data'] = [];
                                resultsNew['status'] = false;
                                return res.json(resultsNew);

                            } else {
                                resultsNew['message'] = "Login Successfully."
                                resultsNew['data'] = results[0];
                                resultsNew['status'] = true;
                                return res.json(resultsNew);
                            }
                        } else {
                            resultsNew['message'] = "Email Is Not Valid";
                            resultsNew['data'] = [];
                            resultsNew['status'] = false;
                            return res.json(resultsNew);
                        }
                    }
                });
            } else {
                var resultsNew = {}
                resultsNew['message'] = "password and username require";
                resultsNew['data'] = [];
                resultsNew['status'] = false;
                return res.json(resultsNew);
            }
        } catch (e) {
            console.log(e);
            return false;
        }
    },
    userLogin: function (req, res) {
        try {
            var uEmail = req.body.uEmail;
            var userPass = req.body.uPassword;
            if (userPass && uEmail) {
                var resultsNew = {}
                db.query('SELECT * FROM user WHERE uEmail="' + uEmail + '" AND status = 1', function (error, results, fields) {
                    if (error) {
                        resultsNew['token'] = "";
                        resultsNew['data'] = {};
                        resultsNew['message'] = error;
                        resultsNew['status'] = false;
                        return res.json(resultsNew);
                    } else {
                        if (results && results[0]) {
                            var userPassword = CryptoJS.AES.decrypt(results[0].uPassword, results[0].userUid).toString(CryptoJS.enc.Utf8);
                            if (userPassword !== userPass) {
                                resultsNew['message'] = "Incorrect Password Entered."
                                resultsNew['data'] = {};
                                resultsNew['status'] = false;
                                return res.json(resultsNew);

                            } else {
                                const token = createJWT(results[0]);
                                req.session.token = token;
                                req.session.user = results[0];
                                req.session.user.loginTime = Date.now();
                                req.session.user.environment = req.app.get('env');
                                //console.log("loginnnnnnnnnnnnnnnnn",results[0]);

                                resultsNew['message'] = "Login Successfully."
                                resultsNew['data'] = results[0];
                                resultsNew['token'] = token;
                                resultsNew['status'] = true;
                                return res.json(resultsNew);
                            }
                        } else {
                            resultsNew['message'] = "Email Is Not Valid";
                            resultsNew['data'] = {};
                            resultsNew['status'] = false;
                            return res.json(resultsNew);
                        }
                    }
                });
            } else {
                var resultsNew = {}
                resultsNew['message'] = "password and username require";
                resultsNew['data'] = {};
                resultsNew['status'] = false;
                return res.json(resultsNew);
            }
        } catch (e) {
            console.log(e);
            return false;
        }
    },
    sendOtp: function (req, res) {
        try {
            const request = require('request');
            var authOtp = Math.floor(100000 + Math.random() * 999999);
            var message = 'Your OTP is :' + authOtp;
            var mobileNo = '9594848572';
            var smsUrl = 'https://api.msg91.com/api/sendotp.php?authkey=287003AmegNnoJrZe5d3bd28e&mobile=' + mobileNo + '&message=' + message + '&sender=DASHCN1&otp=' + authOtp;
            var resultsNew = {};
            request(smsUrl, function (error, response, body) {
                var sqlLog = "INSERT INTO logs(`lId`, `mobile_email`, `otp`, `timestamp`, `expired`,`resp`) VALUES('','" + mobileNo + "','" + authOtp + "',NOW(),'0','" + message + "')";
                db.query(sqlLog, function (error, results, fields) {
                    if (error) {
                        resultsNew['token'] = "";
                        resultsNew['data'] = "";
                        resultsNew['error'] = error;
                        resultsNew['status'] = false;
                    } else {
                        resultsNew['userName'] = userName;
                        resultsNew['authOtp'] = authOtp;
                        resultsNew['data'] = results;
                        resultsNew['status'] = true;
                    }
                    res.json(resultsNew);
                });
            });
            // res.json(resultsNew);
        } catch (e) {
            console.log(e);
        }
    },
    verifyOtp: function (req, res) {
        try {
            var usertype = req.body.usertype;
            var authOtp = req.body.otp;
            if (authOtp && usertype) {
                var resultsNew = {};
                var sqlOtp = "select * from logs where otp='" + authOtp + "' and expired='0'";
                db.query(sqlOtp, function (error, maiResults, fields) {
                    if (error) {
                        resultsNew['data'] = "";
                        resultsNew['error'] = error;
                        resultsNew['status'] = false;
                        res.json(resultsNew);
                    } else {
                        if (maiResults && maiResults[0]) {
                            var selectUser = "select * from users where contact='" + maiResults[0]['mobile_email'] + "' ";
                            db.query(selectUser, function (error, results, fields) {
                                if (results && results[0]) {
                                    resultsNew['message'] = "Data not found";
                                    // resultsNew['data'] = getEncryptPass('admin123');
                                    resultsNew['status'] = false;
                                    res.json(resultsNew);
                                } else {
                                    var insUser = "INSERT INTO users(`uId`, `uName`, `contact`, `country`, `state`, `city`, `location`, `createdBy`, `updatedBy`, `createdDate`, `updatedDate`, `uType`, `status`) VALUES('','GUEST','" + maiResults[0]['mobile_email'] + "','','','','','','',NOW(),NOW(),'" + usertype + "','1')";
                                    db.query(insUser, function (error, results, fields) {
                                        if (results) {
                                            var update = "update logs set expired='1' where otp='" + authOtp + "'";
                                            db.query(update, function (error, results, fields) {
                                                if (results && results) {
                                                    var selectUser = "SELECT * FROM users where contact='" + maiResults[0]['mobile_email'] + "'";
                                                    db.query(selectUser, function (error, results, fields) {
                                                        if (results && results[0]) {
                                                            // var accessToken = jwt.sign({ username: results[0]['User_Name'] }, config.jwt_secret);
                                                            // resultsNew['token'] = accessToken;
                                                            resultsNew['data'] = results[0];
                                                            resultsNew['status'] = true;
                                                            res.json(resultsNew);
                                                        } else {
                                                            resultsNew['message'] = "Data not found";
                                                            resultsNew['status'] = false;
                                                            res.json(resultsNew);
                                                        }
                                                    });
                                                } else {
                                                    resultsNew['message'] = "Data not found";
                                                    resultsNew['status'] = false;
                                                    res.json(resultsNew);
                                                }
                                            });
                                        } else {
                                            resultsNew['message'] = "Data not found";
                                            resultsNew['status'] = false;
                                            res.json(resultsNew);
                                        }
                                    });

                                }
                            });
                        } else {
                            resultsNew['message'] = "Data not found";
                            // resultsNew['data'] = getEncryptPass('admin123');
                            resultsNew['status'] = false;
                            res.json(resultsNew);
                        }
                    }
                });
            } else {
                var resultsNew = {}
                resultsNew['message'] = "password and username require";
                resultsNew['data'] = [];
                resultsNew['status'] = false;
                res.json(resultsNew);
            }
        } catch (e) {
            console.log(e);
        }
    },

    userRegistration: function (req, res) {
        try {
            var userName = req.body.uName;
            var email = req.body.uEmail;
            var uMobile = req.body.uMobile;
            var password = req.body.uPassword;
            var confirmPassword = req.body.ucPassword;
            var createdBy = req.body.createdBy ? req.body.createdBy : 0;
            var updateBy = req.body.updateBy ? req.body.updateBy : 0;
            var status = req.body.status ? req.body.status : "1";
            var resultsNew = {};
            if (password == confirmPassword) {

                db.query('SELECT * FROM user WHERE uEmail="' + email + '" AND status = 1', function (error, results) {
                    if (error) {
                        resultsNew['data'] = {};
                        resultsNew['message'] = error;
                        resultsNew['status'] = false;
                    }
                    if (results && results.length) {
                        resultsNew['data'] = {};
                        resultsNew['message'] = "Email Already Exists";
                        resultsNew['status'] = false;
                        return res.json(resultsNew);
                    } else {
                        const uuid = randomString({
                            length: 10
                        });
                        const userpassword = CryptoJS.AES.encrypt(password, uuid).toString();

                        var insData = 'INSERT INTO `user`(`uName`, `uEmail`, `uCity`, `uState`, `uMobile`, `uPassword`, `userUid`, `uType`, `uCreatedBy`, `uUpdatedBy`, `uCreatedDate`, `uUpdatedDate`, `status`) VALUES ("' + userName + '","' + email + '","","","'+uMobile+'","' + userpassword + '","' + uuid + '","D","' + createdBy + '","' + updateBy + '",NOW(),NOW(),"' + status + '")';

                        db.query(insData, function (error, results) {
                            if (error) {
                                resultsNew['data'] = {};
                                resultsNew['message'] = error;
                                resultsNew['status'] = false;
                            } else {
                                if (results) {
                                    resultsNew['data'] = results;
                                    resultsNew['message'] = "Successfully Registered";
                                    resultsNew['status'] = true;
                                } else {
                                    resultsNew['message'] = "Data not found";
                                    resultsNew['data'] = {};
                                    resultsNew['status'] = false;
                                }
                            }
                            return res.json(resultsNew);
                        });
                    }
                });

            } else {
                resultsNew['message'] = "Password And Confirm Password Not Same";
                resultsNew['data'] = {};
                resultsNew['status'] = false;
                return res.json(resultsNew);
            }
        } catch (e) {
            console.log("########### catch error ###########", e);
        }

    },
    forgotPassword: function (req, res) {
        try {
            var userEmail = req.body.uEmail ? req.body.uEmail : "";
            db.query('SELECT * FROM user WHERE uEmail = "' + userEmail + '"', function (error, results) {
                var resultsNew = {}
                if (error) {
                    resultsNew['message'] = error;
                    resultsNew['data'] = [];
                    resultsNew['status'] = false;
                    res.json(resultsNew);
                } else {
                   
                    if (results && results[0]) {
                        //console.log("1----",results[0]);
                        var transporter = nodemailer.createTransport(config.smtp);
                        mailOptions = {
                            from: 'vinaybaghel180@gmail.com',
                            to: 'marutikarad206@gmail.com',
                            subject: 'Forgot Password',
                            text: 'Password Is :'
                        };

                        transporter.sendMail(mailOptions, function (error, info) {
                            console.log("4----");
                            if (error) {
                                resultsNew['message'] = error;
                                resultsNew['data'] = [];
                                resultsNew['status'] = false;
                            } else {
                                resultsNew['message'] = "send mail";
                                resultsNew['data'] = info;
                                resultsNew['status'] = true;
                            }
                            res.json(resultsNew);
                        });

                    } else {
                        console.log("3----");
                        resultsNew['message'] = "EmailId is Not Valid";
                        resultsNew['data'] = [];
                        resultsNew['status'] = false;
                        res.json(resultsNew);
                    }
                }
            });
        } catch (e) {
            console.log(e);
        }

    },
    userProfile: function (req, res) {
        try {
           
            var id = req.decoded.uId;
            var uName = req.body.uName ? req.body.uName : "";
            var uEmail = req.body.uEmail ? req.body.uEmail : "";
            var uMobile = req.body.uMobile ? req.body.uMobile : "";
            var uCity = req.body.uCity ? req.body.uCity : "";
            var uState = req.body.uState ? req.body.uState : "";
         
            if (id && id != "") {
                var resultsNew = {}
                db.query('SELECT * FROM user WHERE uId="' + id + '" AND status = 1', function (error, results, fields) {
                    if (error) {
                        resultsNew['data'] = [];
                        resultsNew['message'] = error;
                        resultsNew['status'] = false;
                    } else {
                        if (results && results[0]) {
                            var resultsNew = {};
                            var updateQuery = 'UPDATE `user` SET `uName`="' + uName + '",`uEmail`="' + uEmail + '",`uMobile`="' + uMobile + '",`uCity`="' + uCity + '",`uState`="' + uState + '",`uUpdatedDate`=NOW(),`uUpdatedBy`="'+id+'",`status`=1 WHERE uId="' + id + '"'
                            db.query(updateQuery, function (error, results1, fields) {
                                if (error) {
                                    resultsNew['data'] = [];
                                    resultsNew['message'] = error;
                                    resultsNew['status'] = false;
                                } else {
                                    if (results1) {
                                        resultsNew['data'] = results1;
                                        resultsNew['status'] = true;
                                    } else {
                                        resultsNew['message'] = "Data not found";
                                        resultsNew['data'] = [];
                                        resultsNew['status'] = false;
                                    }

                                }
                                return res.json(resultsNew);

                            });
                        } else {
                            resultsNew['message'] = "Data not found";
                            resultsNew['data'] = [];
                            resultsNew['status'] = false;
                            return res.json(resultsNew);
                        }

                    }

                });
            } 
        } catch (e) {
            console.log(e);
            return false;
        }
    },

    changePassword: function(req, res) {
        try {
            var id = req.decoded.uId;
            var password = req.body.password;
            var newPassword = req.body.newPassword;
            var confirmPassword = req.body.confirmPassword;
           
            var resultsNew = {};
            if (newPassword == confirmPassword) {
    
                db.query('SELECT * FROM user WHERE uId="' + id + '" AND status = 1', function(error, results) {
                    if (error) {
                        resultsNew['data'] = {};
                        resultsNew['message'] = error;
                        resultsNew['status'] = false;
                    }
                    if (results && results.length) {
                        var passwordDescript = CryptoJS.AES.decrypt(results[0].uPassword, results[0].userUid).toString(CryptoJS.enc.Utf8);
                        if (passwordDescript !== password) {
                            resultsNew['message'] = "Incorrect Password Entered."
                            resultsNew['data'] = {};
                            resultsNew['status'] = false;
                            return res.json(resultsNew);
    
                        } else {

                            const uuid = randomString({
                                length: 10
                            });

                            const userpassword = CryptoJS.AES.encrypt(newPassword, uuid).toString();
                            var updateQuery = 'UPDATE `user` SET `uPassword`="' + userpassword + '",`userUid`="' + uuid + '",`uUpdatedDate`=NOW(),`uUpdatedBy`="'+id+'",`status`=1 WHERE uId="' + id + '"'
                            db.query(updateQuery, function(error, result1) {
                                if (error) {
                                    resultsNew['data'] = {};
                                    resultsNew['message'] = error;
                                    resultsNew['status'] = false;
                                } else {
                                    if (result1) {
                                        resultsNew['data'] = result1;
                                        resultsNew['message'] = "Successfully Password Changed";
                                        resultsNew['status'] = true;
                                    } else {
                                        resultsNew['message'] = "Data not found";
                                        resultsNew['data'] = {};
                                        resultsNew['status'] = false;
                                    }
                                }
                                return res.json(resultsNew);
                            });
    
                        }
                    }
                });
    
            } else {
                resultsNew['message'] = "New Password And Confirm Password Not Same";
                resultsNew['data'] = {};
                resultsNew['status'] = false;
                return res.json(resultsNew);
            }
        } catch (e) {
            console.log("########### catch error ###########", e);
        }
    
    }

};