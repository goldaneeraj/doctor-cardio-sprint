/**
 * @author : vinay singh
 * @description Configuration file of App

/*********************************
CORE PACKAGES
**********************************/
var express = require('express');
var router = express.Router();
var q = require('q');
var fs = require('fs');
// var piexif = require("piexifjs");
// var Knox = require('knox');
// var moment = require('moment');
// var crypto = require('crypto');
// var cmd = require("cmd-exec").init();
// var auth = require('../config/auth.js');
var async = require('async');
var request = require('request');
var busboy = require('connect-busboy');
var config = require('../config.js');
// var aws = require('aws-sdk');
var Imagemin = require('imagemin');
require('dotenv').config()
const path = require('path');
var Promise = require('promise');
var unzip = require('unzipper');

router.use(busboy());

// var bucket_path = "myBucket";

var bucket_Url = "/";
var local_bucket_path = "/";

router.post('/upload', function (req, res) {
    var hostname = req.headers.host;
    req.pipe(req.busboy);
    var fields = {};
    req.busboy.on('field', function (key, value, keyTruncated, valueTruncated) {
        fields[key] = value;
    });
    req.busboy.on('file', function (fieldname, file, filename) {
        var filename_new = filename.replace(".zip", "");
        filename_new = filename_new.replace(".ZIP", "");
        filename_new = filename_new.replace(" (1)", "");
        filename_new = filename_new.replace("(1)", "");
        filename = filename.replace(/\s/g, '');
        var today = new Date();
        
        var myFileName = (today.getMonth() + 1) + '-' + today.getDate() + '-' + today.getFullYear() + '_' + today.getHours() + '-' + today.getMinutes();
        filename = myFileName + '_' + filename;
        // fs.chmodSync("public/uploads", 0777);
        var local_file_path = "public/uploads/" + fields['path'] + '/';
        // var local_file_path = fields['path'] + '/';
        if (!fs.existsSync(local_file_path)) {
            fs.mkdirSync(local_file_path);
        }
        // fs.chmodSync(local_file_path, 0777);
        
        var fstream = fs.createWriteStream(local_file_path + filename);
        file.pipe(fstream);
        fstream.on('close', function (err, result) {
            if(fields['path'] == "eLearningPackage") {
                var dirPath  = local_file_path + filename;
                var date = new Date();
                var components = [
                    date.getYear(),
                    date.getMonth(),
                    date.getDate(),
                    date.getHours(),
                    date.getMinutes(),
                    date.getSeconds(),
                    date.getMilliseconds()
                ];

                var unzipFolderName = components.join("");
                
                var destPath = "public/uploads/"  + unzipFolderName;
                if (!fs.existsSync(destPath)) {
                    fs.mkdirSync(destPath);
                }
                fs.createReadStream(dirPath).pipe(unzip.Extract({ path: destPath }));
                var destPath = hostname+"/uploads/"  + unzipFolderName + "/" + filename_new + "/presentation_html5.html";
            }else{
                var destPath = hostname+'/uploads/'+fields['path'];
            }
    
            
            res.json({
                message: 'success',
                url: hostname+'/uploads/'+fields['path'] + '/' + filename,
                folderName: destPath
            });
        });
    });
});

module.exports = router;
