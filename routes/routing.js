/*********************************
CORE PACKAGES
**********************************/
var express = require('express');
// const debug = require('debug')('ERP:server');
var router = express.Router();

/*********************************
MODULE PACKAGES
**********************************/
var doctors = require('../routes/doctors.js');
router.use('/doctors', doctors);

var userApi = require('../routes/userApi.js');
router.use('/userApi', userApi);

var upload = require('../routes/upload.js');
router.use('/upload', upload);

module.exports = router;
