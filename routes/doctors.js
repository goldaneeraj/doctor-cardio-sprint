var express = require('express');
var router = express.Router();
 
/*********************************
GET REQUESTS
**********************************/
router.get('/', function(req, res, next) {
    if (req.session.user) {
        res.render('doctors/list', {
            "username": req.session.user,
            "title": "Dashboard",
            "menu": "",
            "result":[]
        });

    } else {
        res.redirect("/");
    }
});

router.get('/form', function(req, res, next) {
    if (req.session.user) {
        res.render('doctors/form', {
            "username": req.session.user,
            "title": "Doctor Form",
            "menu": "",
            "result":[]
        });
    } else {
        res.redirect("/");
    }
});

module.exports = router;
