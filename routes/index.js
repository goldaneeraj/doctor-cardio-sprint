/*********************************
CORE PACKAGES
**********************************/
var express = require('express');
var router = express.Router();
// var mysql = require('mysql');
var _ = require("underscore");
var crypto = require('crypto');
const CryptoJS = require('crypto-js');
const randomString = require('random-string');

/*********************************
MODULE PACKAGES
**********************************/
var config = require('../config');
var db = require('../dbConnection');
var controller = require('../controllers/userController.js');
// var baseExport = require('../baseExporter.js');

function getEncryptPass(str){
	var mykey = crypto.createCipher('aes-128-cbc', str);
	var mystr = mykey.update('admin', 'utf8', 'hex')
	return mystr += mykey.final('hex');
}
 
/*********************************
GET REQUESTS
**********************************/ 

router.get('/', function(req, res, next) {
	// console.log(req.session);
	if(req.session && req.session.user){
		res.redirect('/index');
	}else{
		res.render('login', { title: 'Express',result : [] });	
	}
});

router.get('/index',  function(req, res) {
	// console.log(req.session.user);
    if (req.session.user) {
        res.render('index', {
            "username": req.session.user,
            "title": "Dashboard",
            "menu": ""
        });

    } else {
        res.render("login");
    }
});

router.get('/form',  function(req, res) {
    // console.log(req.session.user);
    if (req.session.user) {
        res.render('form', {
            "username": req.session.user,
            "title": "Form Page",
            "menu": ""
        });

    } else {
        res.render("login");
    }
});

router.get('/table',  function(req, res) {
    // console.log(req.session.user);
    if (req.session.user) {
        res.render('table', {
            "username": req.session.user,
            "title": "List Page",
            "menu": ""
        });

    } else {
        res.render("login");
    }
});

router.post('/addUser', (req, res) => {
	console.log(req.body);
	controller.addUser(req, res);
});

router.post('/loginPassWeb', (req, res) => {
	console.log(req.body);
	// controller.loginPass(req, res);
	try {
        var uEmail = req.body.uEmail;
        var userPass = req.body.uPassword;
        if (userPass && uEmail) {
            // var userPass = getEncryptPass(userPass);
            console.log("userPass---  ", userPass);
            var resultsNew = {}
            db.query('SELECT * FROM user WHERE uEmail="' + uEmail + '" AND uType="A" AND status = 1', function (error, results, fields) {
                if (error) {
                    resultsNew['token'] = "";
                    resultsNew['data'] = req.body;
                    resultsNew['message'] = error;
                    resultsNew['status'] = false;
                    // return res.json(resultsNew);
                    console.log(resultsNew);
                    res.render('login', { title: 'Express',result : resultsNew });  
                } else {
                    if (results && results[0]) {
                        var userPassword = CryptoJS.AES.decrypt(results[0].uPassword, results[0].userUid).toString(CryptoJS.enc.Utf8);
                        if (userPassword !== userPass) {
                            resultsNew['message'] = "Incorrect Password Entered."
                            resultsNew['data'] = req.body;
                            resultsNew['status'] = false;
                            // return res.json(resultsNew);
                            console.log(resultsNew);
                            res.render('login', { title: 'Express',result : resultsNew });  
                        } else {
                            resultsNew['message'] = "Login Successfully."
                            resultsNew['data'] = results[0];
                            resultsNew['status'] = true;
                            // return res.json(resultsNew);
                            req.session.token = "token";
		                    req.session.user = results[0];
		                    req.session.user.loginTime = Date.now();
		                    // req.session.user.environment = req.app.get('env');
		                    sess = req.session;
		                    res.redirect('/');
                        }
                    } else {
                        resultsNew['message'] = "Email Is Not Valid";
                        resultsNew['data'] = req.body;
                        resultsNew['status'] = false;
                        // return res.json(resultsNew);
                        console.log(resultsNew);
                        res.render('login', { title: 'Express',result : resultsNew });  
                    }
                }
            });
        } else {
            var resultsNew = {}
            resultsNew['message'] = "password and username require";
            resultsNew['data'] = req.body;
            resultsNew['status'] = false;
            // return res.json(resultsNew);
            console.log(resultsNew);
            res.render('login', { title: 'Express',result : resultsNew });  
        }
    } catch (e) {
        console.log(e);
        return false;
    }
});

router.post('/loginPass', (req, res) => {
	console.log(req.body);
	controller.loginPass(req, res);
});

router.get('/logout', function (req, res) {
    req.session.destroy(function () {
        res.redirect('/');
    });
});

/*
router.post('/gridData', function(req, res, next) {
	// console.log("------------hiii-------------");
	var query = {};
    var limit = req.body.limit ? parseInt(req.body.limit) : 50;
    var search_by = req.body.search_by ? req.body.search_by : "";
    var sort_by = req.body.sort_by ? req.body.sort_by : "id";
    var order = req.body.order ? req.body.order : "desc";
    var page = req.body.page ? parseInt(req.body.page) : 0;
    var columns = req.body.columns ? req.body.columns : [];
    var filter_columns = {};
    var draw = req.body.draw ? parseInt(req.body.draw) : 1;
    var start = req.body.start ? parseInt(req.body.start) : 0;
    if(req.body.search_query){
        var search_query = req.body.search_query;
        // if(search_query.branch){ query.branch = search_query.branch; }
    }
    const sql = "SELECT * FROM `users` order by "+sort_by+" "+order+" limit "+ start +","+limit;
    db.query({ sql }, function (error, users, fields) {
    	if(users){
			res.json({
	            count: users.length,
	            result: users,
	            limit: limit,
	            start: limit * page,
	            draw: draw, 
	            page: page
	        });
    	}else{
    		res.sendStatus(401);
    	}
    });
});
*/

/*********************************
POST REQUESTS
**********************************/


module.exports = router;