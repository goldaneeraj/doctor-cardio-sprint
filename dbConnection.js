var test = "";
var mysql = require('mysql');
var db;
function connectDatabase() {
    if (!db) {

        db = mysql.createConnection({
            host: "localhost", //172.17.0.2 localhost
            user: "root",
            password: "", 
            database : 'lms' 
        });
        db.connect(function(err) {
            if (err){
                console.log("*********FAILD***********");
                console.log(err);
            }
            else{
                console.log("Connected!");
            }
        });
    }
    return db; 
}

module.exports = connectDatabase();
